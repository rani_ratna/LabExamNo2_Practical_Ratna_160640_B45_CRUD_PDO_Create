<?php


namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class City extends DB
{

    private $id;
    private $name;
    private $city;


    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)){
            $this->name=$allPostData['name'];
        }

        if(array_key_exists("city_name",$allPostData)){
            $this->city=$allPostData['city_name'];
        }
    }

    public  function store(){
        $arrData=array($this->name,$this->city);

        $query= "INSERT INTO city (name,city) VALUES (?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Data has been Stored Successfully!");
        }

        else {
            Message::setMessage("Failed!Data has not been Stored.");
        }

        Utility::redirect('create.php');
    }
}