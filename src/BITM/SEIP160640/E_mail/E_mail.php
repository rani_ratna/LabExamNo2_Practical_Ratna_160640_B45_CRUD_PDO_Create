<?php


namespace App\E_mail;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class E_mail extends DB
{

    private $id;
    private $name;
    private $email;


    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)){
            $this->name=$allPostData['name'];
        }

        if(array_key_exists("email",$allPostData)){
            $this->email=$allPostData['email'];
        }
    }

    public  function store(){
        $arrData=array($this->name,$this->email);

        $query= "INSERT INTO email (name,email) VALUES (?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Data has been Stored Successfully!");
        }

        else {
            Message::setMessage("Failed!Data has not been Stored.");
        }

        Utility::redirect('create.php');
    }
}