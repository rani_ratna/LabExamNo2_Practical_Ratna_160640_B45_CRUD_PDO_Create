<?php
/**
 * Created by PhpStorm.
 * User: RATAN
 * Date: 1/28/2017
 * Time: 10:10 AM
 */

namespace App\BirthDay;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class BirthDay extends DB
{
    private $id;
    private $name;
    private $dateOfBirth;


    public  function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id=$allPostData['id'];
        }

        if(array_key_exists("name",$allPostData)){
            $this->name=$allPostData['name'];
        }

        if(array_key_exists("dob",$allPostData)){
            $this->dateOfBirth=$allPostData['dob'];
        }
    }

    public  function store(){
        $arrData=array($this->name,$this->dateOfBirth);

        $query= "INSERT INTO dob(name,dob) VALUES (?,?)";
        $STH= $this->DBH->prepare($query); //STH=Statement Handle
        $result= $STH->execute($arrData);

        if($result){
            Message::setMessage("Data has been Stored Successfully!");
        }

        else {
            Message::setMessage("Failed!Data has not been Stored.");
        }

        Utility::redirect('create.php');
    }

}