<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if( (!isset($_SESSION)))
    session_start();
    $msg= Message::getMessage();
if($msg)
    {
    echo "<div class='footerimg'>$msg </div>";
    $_SESSION['message'] = "";
    }

    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hobbies</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">
        <style>
            .checked {

                width: 25px;
                margin-top: 5px;
                margin-right:5px;
                margin-left: 5px;
                height: 22px;
            }

            .footerimg {
                 position: relative;
                 top: 90%;
                 left: 45%;
                 margin: -150px 0 0 -130px;
                 width:400px;
                 height:45px;
                 color: #ffffff;
                 font-family: 'Lobster', helvetica, arial;
                 font-size: 17px;

             }


        </style>
    </head>
    <body>
    <div class="container">
    <form action="store.php" method="post">
        <h1>Select Hobbies </h1>
        <input type="text" name="name" placeholder="Enter Your Name"><br>
        <div class="radio-toolbar-1">
            <label><input type="checkbox" name="hobby[]" class="checked" value="Photography">Photography</label><br><br>
            <label><input type="checkbox" name="hobby[]" class="checked" value="Gardening">Gardening</label><br><br>
            <label><input type="checkbox" name="hobby[]" class="checked" value="Watch Movie">Watch Movie</label><br><br>
            <label><input type="checkbox" name="hobby[]" class="checked" value="Reading Book">Reading Book</label><br><br>
        </div>

        <input type="submit" value="Add"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>

    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
        })
    </script>
    </body>
    </html>

