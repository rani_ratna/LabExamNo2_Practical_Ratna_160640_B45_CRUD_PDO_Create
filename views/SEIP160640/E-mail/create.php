<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if( (!isset($_SESSION)))
    session_start();
    $msg= Message::getMessage();
if($msg)
    {
    echo "<div class='footer'>$msg </div>";
    $_SESSION['message'] = "";
    }

    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>E-mail</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">

    </head>
    <body>
    <div class="container">
    <form action="store.php" method="post">
        <h1>Add E-mail </h1>
        <input type="text" name="name" placeholder="Enter Your Name"><br>
        <input type="text" name="email" placeholder="Enter E-mail Id"><br>
        <input type="submit" value="Add"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>

    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
            $('.footer').fadeIn(550);
            $('.footer').fadeOut(550);
        })
    </script>
    </body>
    </html>

