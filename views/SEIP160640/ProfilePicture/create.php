<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION))

    session_start();
    $msg= Message::getMessage();
    if($msg) {
        echo "<div class='footerimg'>$msg </div>";

        $_SESSION['message'] = "";
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Profile Picture</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">
        <style> .footerimg {
        position: relative;
        top: 100%;
        left: 45%;
        margin: -150px 0 0 -130px;
        width:400px;
        height:45px;
        color: #ffffff;
        font-family: 'Lobster', helvetica, arial;
        font-size: 17px;

        }</style>
        <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
        <script>
            function imagepreview(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#imgpreview').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

        </script>
    </head>
    <body>
    <div class="container">
    <form action="store.php" method="post" enctype="multipart/form-data">
        <h1>Profile Picture </h1>
        <input type="text" name="name" placeholder="Enter Name"><br>
        <div class="profilePic"><img id="imgpreview" width="420px" height="250px"/></div>
        <input type="file" name="profilePic" onchange="imagepreview(this)">
        <input type="submit" value="Upload"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>
    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
        })
    </script>
    </body>
    </html>

