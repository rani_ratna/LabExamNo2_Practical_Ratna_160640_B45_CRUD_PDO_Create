<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;

if( (!isset($_SESSION)))
    session_start();
$msg= Message::getMessage();
if($msg) {
    echo "<div class='footerimg'>$msg </div>";

    $_SESSION['message'] = "";
}

    ?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Gender</title>
        <link rel="stylesheet" href="../../../resource/css/formstyle.css">
        <style> .footerimg {
                position: relative;
                top: 90%;
                left: 45%;
                margin: -150px 0 0 -130px;
                width:400px;
                height:45px;
                color: #ffffff;
                font-family: 'Lobster', helvetica, arial;
                font-size: 17px;

            }</style>
    </head>
    <body>
    <div class="container">
    <form action="store.php" method="post">
        <h1>Gender Information </h1>
        <input type="text" name="name" placeholder="Enter Name"><br>
        <div class="radio-toolbar">
            <label><input type="radio" name="gender" value="Male" >Male</label><br><br><br>
            <label><input type="radio" name="gender" value="Female" >Female</label>
        </div>
        <input type="submit" value="Add"  name="btnAdd" class="button">
        <input type="submit" value="Add & Save" class="button">
        <input type="submit" value="Reset" class="button">
    </form>
        </div>
    <script type="text/javascript" src="../../../resource/js/jquery-3.1.1.min.js"></script>
    <script>

        jQuery (function($){

            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
            $('.footerimg').fadeIn(550);
            $('.footerimg').fadeOut(550);
        })
    </script>
    </body>
    </html>

